name 'carneiro_users'
maintainer 'The Authors'
maintainer_email 'you@example.com'
license 'All Rights Reserved'
description 'Installs/Configures carneiro_users'
long_description 'Installs/Configures carneiro_users'
version '0.1.0'
chef_version '>= 12.14' if respond_to?(:chef_version)

depends 'users', '~> 5.3.1'
