##Do mais "fraco" para o mais "forte" em um único ponto (recipe, por exemplo):
## default force_default normal override force_override automatic

node.force_override['ipaddress'] = '127.0.0.1' # Este nao funciona, pois vem do automatic, que tem maior força
node.normal['catch_phrase'] = 'Welcome to the kitchen - on attribute file'
node.force_default['catch_phrase'] = 'Cooking with Gas! - on attribute file' # Force default é mais fraco que o "normal", logo, isso nao vai sobrescrever nada...

