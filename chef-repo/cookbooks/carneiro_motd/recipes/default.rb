#Existem alguns metodos para editar os atributos de um nó
#Estes atributos e metodos tem uma serie de preferencias
#veja: https://docs.chef.io/_images/overview_chef_attributes_table.png
#
##Do mais "fraco" para o mais "forte" em um único ponto (recipe, por exemplo):
## default force_default normal override force_override automatic
#
node.force_override['ipaddress'] = '127.0.0.1' # Este nao funciona
node.normal['catch_phrase'] = 'Welcome to the kitchen'
node.force_default['catch_phrase'] = 'Cooking with Gas!' # Force default é mais fraco que o "normal", logo, isso nao vai sobrescrever nada...

# para gerar um "attribute file" faca:
# chef generate attribute cookbooks/carneiro_motd <NomeDoAttribute... no caso, "default">



file '/etc/motd' do
	content "IP Address: #{node['ipaddress']}
Catch Phrase: #{node['catch_phrase']}"
end

