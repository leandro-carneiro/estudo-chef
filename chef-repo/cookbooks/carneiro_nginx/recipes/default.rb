package 'epel-release'

package 'nginx'

service 'nginx' do
  action [:enable, :start]
  subscribes :reload, 'file[/etc/nginx/example.conf]', :delayed
end

file '/usr/share/nginx/html/index.html' do
  content '<h1>Hello world, from Chef</h1>'
  action :create
  not_if { ::File.exists?('/usr/share/nginx/html/index.html') } # Test if file exists in pure ruby
end

file '/etc/nginx/example.conf' do
	content ''
end

###### Transferido para os helpers (biblioteca) #######
#file_contents = "<ul>"
#
##As opções para busca são:
## client | <data bag name> | environment | node | role
#search(
#	'environment', # Opcao da Busca
#	'name:*',      # Filtro (qualquer campo do Json)
#	{ 
#		filter_result: # Como vai mapear os resultados
#		{
#			'name' => ['name'],        
#			'desc' => ['description'] # env['desc'] => Json "description"
#		} 
#	} 
#).each do |env|
#	puts("*** Environment is: #{env} ***")
#	file_contents += "<li>Name: #{env['name']} - #{env['desc']}</li>\n"
#end
#
#file_contents += "</ul>"
####################################################

file '/usr/share/nginx/html/environments.html' do
	extend CarneiroNginx::EnvironmentsHelpers
	content environments_html_list('name:*')
end
