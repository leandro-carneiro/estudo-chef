#
# Chef Documentation
# https://docs.chef.io/libraries.html
#

#
# Thi module name was auto-generated from the cookbook name. This name is a
# single word that starts with a capital letter and then continues to use
# camel-casing throughout the remainder of the name.
#
module CarneiroNginx
  module EnvironmentsHelpers
    #
    # Define the methods that you would like to assist the work you do in recipes,
    # resources, or templates.
    #
    # def my_helper_method
    #   # help method implementation
    # end
      def environments_html_list(query = 'name:*')
         file_contents = "<ul>"
         search(
                 'environment', # Opcao da Busca
                 query,      # Filtro (qualquer campo do Json)
                 { 
                         filter_result: # Como vai mapear os resultados
                         {
                                 'name' => ['name'],        
                                 'desc' => ['description'] # env['desc'] => Json "description"
                         } 
                 } 
         ).each do |env|
                 #puts("*** Environment is: #{env} ***")
                 file_contents += "<li>Name: #{env['name']} - #{env['desc']}</li>\n"
         end
         file_contents += "</ul>"
     end
  end
end

#
# The module you have defined may be extended within the recipe to grant the
# recipe the helper methods you define.
#
# Within your recipe you would write:
#
#     extend CarneiroNginx::EnvironmentsHelpers
#
#     my_helper_method
#
# You may also add this to a single resource within a recipe:
#
#     template '/etc/app.conf' do
#       extend CarneiroNginx::EnvironmentsHelpers
#       variables specific_key: my_helper_method
#     end
#
