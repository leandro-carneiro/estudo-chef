# -*- mode: ruby -*-
# vi: set ft=ruby :

LIBVIRT_HOST = ENV['LIBVIRT_HOST'] || "192.168.2.81"

required_plugins = %w(vagrant-hostsupdater vagrant-puppet-install vagrant-libvirt)
required_plugins.each do |plugin|
  system "vagrant plugin install #{plugin}" unless Vagrant.has_plugin? plugin
end

Vagrant.configure("2") do |config|

  machines=[
    #{ :hostname => "leoburnett",  :ipPriv => "192.168.122.11", :ipPub => "192.168.2.11", :box => "lcarneiro/amzn1", :ram => 1512,  :cpu => 1, :mac => "0B" },
    #{ :hostname => "amzn2",  :ipPriv => "192.168.122.10", :ipPub => "192.168.2.10", :box => "lcarneiro/amzn2", :ram => 1512,  :cpu => 1, :mac => "0A" },
    { :hostname => "Server",      :ipPriv => "192.168.122.75", :ipPub => "192.168.2.75", :box => "centos/7", :ram =>8192, :cpu => 4, :mac => "75" },
    { :hostname => "WorkStation", :ipPriv => "192.168.122.76", :ipPub => "192.168.2.76", :box => "centos/7", :ram =>4096, :cpu => 2, :mac => "76" },
    { :hostname => "Node",        :ipPriv => "192.168.122.77", :ipPub => "192.168.2.77", :box => "centos/7", :ram =>4096, :cpu => 2, :mac => "77" },
    #{ :hostname => "isobar-pulman-gcp-vrg-lnx-prd-app-01", :ipPriv => "192.168.122.37", :ipPub => "192.168.2.17", :box => "generic/ubuntu1804", :ram =>1512, :cpu => 1, :mac => "37" },
  ]

  machines.each do |machine|
      #config.puppet_install.puppet_version = :latest
      config.vm.define machine[:hostname] do |node|
          node.vm.network :private_network, :ip => machine[:ipPriv], :mode => "nat"
          if ENV['LIBVIRT_HOST']
            node.vm.network "public_network", :dev => "br0", :bridge => "br0", :ip => machine[:ipPub]
          else
            node.vm.network "public_network", :dev => "br0", :bridge => "br0", :use_dhcp_assigned_default_route => "true", :mac => "52:54:00:FF:FF:" + machine[:mac] #MAC fixo para receber mesmo ip do dhcp
          end
          node.vm.synced_folder ".", "/vagrant", type: "nfs", nfs_udp: false, nfs_version: 3
          #node.vm.synced_folder ".", "/vagrant", type: "rsync"
          node.vm.synced_folder "~/.ssh", "/root/.ssh", type: "rsync"
          node.vm.box = machine[:box]
          node.hostsupdater.aliases = [ machine[:hostname] ]
          node.vm.hostname = machine[:hostname] + ".libvirt"
          node.vm.provider :libvirt do |libvirt|
             libvirt.host = LIBVIRT_HOST
             libvirt.username = "root"
             libvirt.id_ssh_key_file = "/opt/id_rsa_libvirt"
             libvirt.connect_via_ssh = true
             libvirt.storage_pool_name = "HDs"
             libvirt.graphics_type = "vnc"
             libvirt.keymap = "pt-br"
             libvirt.suspend_mode = "managedsave"
             libvirt.features = [ 'acpi', 'apic', 'pae' ]
             libvirt.graphics_ip = '0.0.0.0'
             libvirt.cpu_mode = 'host-passthrough'
            # libvirt.loader = '/usr/share/edk2/ovmf/OVMF_CODE.fd' #Descomente se precisar subir em UEFI
             libvirt.cpus = machine[:cpu]
             libvirt.memory = machine[:ram]
            # libvirt.storage :file, :size => '100G', :type => 'qcow2' #Descomente para criar discos
          end
      end
  end

  config.vm.provision "shell", inline: <<-END_OF_SHELL
    yum install vim tree net-tools -y
    chown root: /root/.ssh -R
    echo "vagrant" | passwd --stdin vagrant
    sed 's/^PasswordAuthentication.*/PasswordAuthentication yes/g' -i /etc/ssh/sshd_config
    systemctl restart sshd

    echo "192.168.122.75 Server.libvirt      server" >> /etc/hosts
    echo "192.168.122.76 WorkStation.libvirt workstation" >> /etc/hosts
    echo "192.168.122.77 Node.libvirt        node" >> /etc/hosts

    if [ "$(hostname -s)" == "Server" ]; then
      rpm -Uvh https://packages.chef.io/files/stable/chef-server/12.17.33/el/7/chef-server-core-12.17.33-1.el7.x86_64.rpm
      chef-server-ctl reconfigure
      chef-server-ctl service-list
      chef-server-ctl user-create leandro Leandro Carneiro leandro@carnei.ro '123456' --filename /opt/leandro.pem
      chef-server-ctl org-create  carneiro-company 'Carneiro Company' --association_user leandro --filename /opt/carneiro-company-validator.pem
      chef-server-ctl install chef-manage 
      chef-server-ctl reconfigure
      chef-manage-ctl reconfigure --accept-license
      ip a | grep "inet " | grep -v 127 | awk '{print $2}' | awk -F'/' '{print "https://"$1}'
    fi

    if [ "$(hostname -s)" == "WorkStation" ]; then
      rpm -Uvh https://packages.chef.io/files/stable/chefdk/2.5.3/el/7/chefdk-2.5.3-1.el7.x86_64.rpm
      #rpm -Uvh https://packages.chef.io/files/stable/chefdk/3.3.23/el/7/chefdk-3.3.23-1.el7.x86_64.rpm
      useradd lcarneiro -G wheel || true
      sed -e 's/^%wheel.*/%wheel        ALL=(ALL)       NOPASSWD: ALL/g' -i /etc/sudoers
      cp -vr /root/.ssh /home/lcarneiro/
      chown lcarneiro: /home/lcarneiro/.ssh -R
      su -l lcarneiro -c '
        chef --version
        echo eval \"\$\(chef shell-init bash\)\" >> ~/.bash_profile
        source ~/.bash_profile
        mkdir -p ~/.chef/
        scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null root@server.libvirt:/opt/leandro.pem ~/.chef/leandro.pem # Pega a chave do usuario do Server
        knife configure --server-url https://Server.libvirt/organizations/carneiro-company --user leandro # Configura o Knife apontando para o Server
        knife ssl fetch # Para pegar o CA do certificado auto assinado
        echo "BootStraping Node1"
        knife bootstrap node.libvirt --node-name node1 --ssh-user vagrant --ssh-password 'vagrant' --sudo
        echo "Here are the node list:"
        knife node list
      '
    fi
    
  END_OF_SHELL

end
